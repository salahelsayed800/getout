//
//  DatumEObject.swift
//  Get Me Out
//
//  Created by Salah  on 07/06/2020.
//  Copyright © 2020 Salah . All rights reserved.
//

import Foundation


struct DatumEObject{
    var id: Int = 0
    var name: String = ""
    var rateAvg: Double = 0.0
    var numOfRater: Int = 0
    var latitude:Double = 0.0
    var longitude: Double = 0.0
    var shortDesc: String = ""
    var address: String = ""
    var imageurl: String = ""
    var isbookmarked: Int = 0
    var dashboardurl: String = ""
}
